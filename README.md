# Gemserv demo

Gemini is a relatively new (2019) protocol for creating simple websites (gemsites, capsules) containing only raw text. It has been created as an answer to current web, which is so bloated and try to be everything, that it's almost impossible to develop a completely new web browser from scratch. Only big corporations have enough resources and money for that. Another not less important issue with current web is privacy – there are many, many different ways websites can track and fingerprint you. There is JavaScript, an arbitrary code downloaded and executed immediately by your browser, without real possibility to control what it does, which have access to many different details about your hardware and software. If you want to see how many information leak from your system and effectively de-anonymize you, go to this website:

* <https://browserleaks.com>

JavaScript also slows down your machine and network connection, so you download more data than needed. For example, reddit.com home page downloads nearly 30 MiB, and even more when you scroll down! Many websites doesn't work without JavaScript which makes them hard to scrap down by indexing bots, e.g. search engines. But even if you disable JavaScript, there are still tracking cookies and supercookies which regenerates itself by calculating hash of User Agent and some other data. There is User-Agent-based browser discrimination. There's content blocking detection, so often extensions protecting your privacy makes you more identifiable.

And the Gemini IS the solution. The two major goals of Gemini are: simplicity and privacy. The specification of this protocol is so simple, that you can read it and hold in your head. And it's easy to make your own Gemini server and browser yourself. Gemini doesn't aim to replace web, but rather coexist peacefully with other technologies and protocols for various different tasks: SSH, BitTorrent, mail, even http(s). In other words, it follows the Unix philosophy. As a demonstration, I've written a very simple Gemini server in PHP with less than 30 lines of code. If you remove the comments and join splitted lines, it will be exactly 18 lines.

## How to run this?

1. Generate key with openssl:

```bash
openssl genrsa -out ssl.key
```

2. Create self-signed x509 certificate

```bash
openssl req -x509 -key ssl.key -out ssl.crt -days (number of days to expiration)
```

3. Run the server

```bash
php server.php
```

4. Open gemini://localhost in your Gemini client. You should see "Hello, geminispace!" message.

* [List of known Gemini clients – I personally recommend Lagrange](https://gemini.circumlunar.space/software)

Gemini requires to all servers use TLS encryption, but signing certificates by CA is not necessarry. It also encourages use of very rarely seen on the web client certificates (public key authentication) instead of insecure passwords.

I think this small script is also a good material for learning basics of direct interaction with TCP. This code is Free Software licenced under GNU GPLv3 or later, feel free to expand it as you wish. Just remember: don't make non-free software. It hurts.

Copyright (C) 2022 anedroid

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

* [Copy of the GNU General Public License](LICENSE) 

––––––––––––––––

If you want to learn more about Gemini and start to explore this awesome universe, here is a good introducing guide:

* <https://geminiquickst.art>

also on Gemini:

* <gemini://geminiquickst.art>
