<?php
$context = stream_context_create();
stream_context_set_option($context, [
	// https://php.net/context.ssl
	'ssl' => [
		'local_cert' => 'ssl.crt',
		'local_pk' => 'ssl.key'
	]
]);
$socket = stream_socket_server("tcp://127.0.0.1:1965", $errno, $errmsg,
	STREAM_SERVER_BIND | STREAM_SERVER_LISTEN, $context);
while (true) {
	$connection = stream_socket_accept($socket, -1, $peer);
	// https://php.net/function.stream-socket-enable-crypto
	stream_socket_enable_crypto($connection, true,
		STREAM_CRYPTO_METHOD_TLS_SERVER &
		~ STREAM_CRYPTO_METHOD_TLSv1_0_SERVER &
		~ STREAM_CRYPTO_METHOD_TLSv1_1_SERVER
	);
	// gemini://gemini.circumlunar.space/docs/specification.gmi
	$input = fread($connection, 1024);
	echo $input;
	fwrite($connection, "20 text/gemini; charset=utf-8\r\n");
	fwrite($connection, "Hello, geminispace!");
	fclose($connection);
}
